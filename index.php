<?php
    /* Домашнее задание к лекции 1.4 «Стандартные функции»

    Для источника данных будем использовать сервис Openweathermap.org. Вся документация по API лежит тут(http://openweathermap.org/current), но если английский язык пока пугает — можно сразу взять данные по ссылке.

    В ссылке указан appid — это ключ преподавателя, который имеет ограничение в 60 запросов в минуту. Из чего следует, что надежнее зарегистрироваться на сервисе и получить свой личный ключ.

    В чем суть задачи: получите данные по ссылке и выведите текущую температуру и погодные условия. Можно выводить погодные условия словами, но, если вы подберете картинки для них, сервис от этого только выиграет.

    Подсказка 1:
    На случай недоступности сервиса лучше, получив первый раз json-данные, сохранить их в текстовый файл, и на период разработки пользоваться такими локальными данными.

    Подсказка 2:
    Если все сделать аккуратно, сервис будет небольшой.

    Факультативно:
    Выведите и другие данные: о давлении, ветре, влажности и прочие, насколько хватит запала.

    Дополнительное задание:
    После запуска, сервис проверяет, есть ли файл с сохраненными данными запроса.
    Если есть и если дата сохранения файла менее часа назад - то используются эти данные (нет доп. запроса)
    Если файла нет или он "старый" - то делается запрос к сервису и сохраняется результат в файл с "кешем".
    */

    //error_reporting(E_ALL);
    //ini_set('display_errors', 1);

    $city = 'Sankt-Peterburg';
    $key = '0be17512ebb10bd737287543d2e4e3bf';
    //$lat = 59.939095; $lon = 30.315868; // СПб
    $tmpUrl = 'http://api.openweathermap.org/data/2.5/find?q=%1&APPID=%2&lang=ru&units=metric';

    //$url = str_replace(['%1', '%2'], [$city, $key], $tmpUrl . '&type=like');
    //$url = str_replace(['%1', '%2', '%3', '%4'], [$city, $key, $lat, $lan], $tmpUrl . '&lat=%3&lon=%4');
    $url = str_replace(['%1', '%2'], [$city, $key], $tmpUrl);

    $fileName = implode(DIRECTORY_SEPARATOR, [__DIR__, 'response.txt']);
    if (file_exists($fileName) && (filemtime($fileName) > time() - 3600)) {
        // Если файл существует, и с момента его изменения прошло менее часа, - берем данные из него
        $content = file_get_contents($fileName);
    } else {
        // Иначе получим данные сервиса и обновим файл
        $content = file_get_contents($url);
        file_put_contents($fileName, $content);
    }

    $decoded = json_decode($content, true);
    if ($decoded['cod'] !== '200' or count($decoded['list']) === 0) {
        $weather = [];
    } else {
        $list = $decoded['list'][0];
        $tagImg = str_replace(['#src', '#alt'],
            ['http://openweathermap.org/img/w/' . $list['weather'][0]['icon'] . '.png', $list['weather'][0]['description']],
            '<img src="#src" alt="#alt">'
        );
        $desDirection = 'c,сз,з,юз,ю,юв,в,св,с'; // deg: 0 - север, 90 - запад, 180 - юг, 270 - восток
        $windDeg = explode(',', $desDirection)[round(($list['wind']['deg']) / 45)];
        $weather = [
            ['Город', $list['name']],
            ['Время', date('Y-m-d H:i:s', $list['dt'])],
            ['Температура', $list['main']['temp'] . '°'],
            ['Погода', $tagImg . '<br>' . $list['weather'][0]['description']],
            ['Давление', round($list['main']['pressure'] * 0.75) . ' мм рт.ст.'],
            ['Влажность', $list['main']['humidity'] . '%'],
            ['Ветер', $list['wind']['speed'] . ' м/с, ' . mb_strtoupper($windDeg)]
        ];
    }

    //echo '<pre>';
    //var_dump($weather);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 1.4</title>
</head>
<body>
    <table>
        <?php foreach($weather as $value) { ?>
            <tr>
                <td><?php echo $value[0] . ':'; ?></td>
                <td><?php echo $value[1]; ?></td>
            </tr>
        <?php } ?>
    </table>
</body>
</html>
